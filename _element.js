import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

/**
 * `google-recaptcha`
 * A captcha using the google library
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class GoogleRecaptcha extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <h2>Hello [[prop1]]!</h2>
    `;
  }
  static get properties() {
    return {
      prop1: {
        type: String,
        value: 'google-recaptcha',
      },
    };
  }
}

window.customElements.define('google-recaptcha', GoogleRecaptcha);
