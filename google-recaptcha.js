import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import {mixinBehaviors} from '@polymer/polymer/lib/legacy/class.js';
import {IronValidatableBehavior} from '@polymer/iron-validatable-behavior/iron-validatable-behavior.js';
import {IronFormElementBehavior} from '@polymer/iron-form-element-behavior/iron-form-element-behavior.js';

import '@polymer/polymer/lib/legacy/polymer.dom.js';

import './iron-scroll-fit.js';
/**
 * `google-recaptcha`
 * A captcha using the google library
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class GoogleRecaptcha extends mixinBehaviors([IronFormElementBehavior, IronValidatableBehavior], PolymerElement) {
  static get template() {
    return html`
      <style>
      :host {
        display: block;
      }
      :host([hidden]) {
        display: none !important;
      }
      :host(:not([invisible])) {
        width: 302px;
        height: 78px;
      }
      :host([compact]) {
        width: 164px;
        height: 144px;
      }
      :host([invisible][badge="inline"]) {
        width: 256px;
        height: 60px;
      }
      </style>

      <iron-scroll-fit
        class="recaptcha-container"
        hidden$="[[hidden]]"
        scroll-target="{{scrollTarget}}"
        horizontal-align="left"
        vertical-align="top">
      </iron-scroll-fit>
    `;
  }
  static get properties() {
    return {
      /**
         * Prevents the recaptcha being moved into the body.
         */
      inBodyDisabled: {
        type: Boolean,
        value: false
      },
      scrollTarget: {
        type: HTMLElement
      },
      /**
         * When true, an invisible recaptcha will be use instead of v2.
         */
      invisible: {
        type: Boolean,
        value: false,
        reflectToAttribute: true
      },
      /**
        * Prevents the recaptcha being automatically rendered on attach.
        */
      manualRender: {
        type: Boolean,
        value: false
      },
      /**
        * True when the recaptcha is rendered.
        */
      rendered: {
        type: Boolean,
        computed: '_computeRendered(widgetId)',
        notify: true
      },
      /**
      * True when the recaptcha library is loaded.
      */
      libraryLoaded: {
        type: Boolean,
        readOnly: true,
        notify: true,
        value: function() {
          return !!window.grecaptcha;
        }
      },

      /**
         * The unique generated id for this recaptcha, if it is rendered.
         */
      widgetId: {
        type: String,
        readOnly: true,
        notify: true
      },

      /**
         * The google recaptcha api key. This value is required for the
         * recaptcha to work. Visit https://www.google.com/recaptcha/admin to
         * get a key and don't forget to configure the allowed domain.
         */
      sitekey: {
        type: String
      },
      /**
       * Not working for now
       */
      loading: {
        type: Boolean,
        value: false
      },
      /**
       * The recaptcha container.
       */
      container: {
        type: HTMLElement
      },
      /**
      * The function called when the user submits a successful CAPTCHA
      * response.
      */
     responseCallback: {
       type: Function,
       value: function() {
         return function(token) {
           this._setValue(token);
           this._setExpired(false);
           this.resolveCompletes(token);
           this.dispatchEvent(new CustomEvent(
             'google-recaptcha-response',
             {detail: token, bubbles: true, composed: true}
           ));
         }.bind(this);
       }
     },
     /**
         * The function called when the recaptcha expires.
         */
        expiredCallback: {
          type: Function,
          value: function() {
            return function() {
              this._setValue('');
              this._setExpired(true);
              this.dispatchEvent(new CustomEvent(
                'google-recaptcha-expired',
                {bubbles: true, composed: true}
              ));
            }.bind(this);
          }
        },
        /**
         * True when the recaptcha has expired and the user needs to solve a
         * new CAPTCHA.
         */
        expired: {
          type: Boolean,
          value: false,
          readOnly: true,
          notify: true
        },
        /**
         * The validated recaptcha token.
         */
        value: {
          type: String,
          readOnly: true,
          notify: true
        },
        /**
         * Use an audio CAPTCHA.
         */
        audio: {
          type: Boolean,
          value: false
        },
        /**
         * Use a compact recaptcha. For v2 only.
         */
        compact: {
          type: Boolean,
          value: false
        },
        /**
         * The recaptcha tabindex
         */
        tabIndex: {
          type: Number,
          value: 0
        },
        /**
         * The recaptcha container z-index when it is in body. Set to `null` to
         * remove the z-index property.
         */
        inBodyZIndex: {
          type: Number
        },
        /**
         * Use the recaptcha dark theme.
         */
        dark: {
          type: Boolean,
          value: false
        },
        /**
         * {bottomright | bottomleft | inline}
         * For invisible only.
         * Reposition the reCAPTCHA badge. 'inline' allows you to control the CSS.
         */
        badge: {
          type: String,
          value: 'bottomright',
          reflectToAttribute: true
        },
        /**
         * A promise that resolves when the grecaptcha response comes back.
         *
         * @type {Promise}
         */
        completes: {
          type: Object,
          readOnly: true,
          notify: true,
          value: function() {
            return new Promise(function(resolve, reject) {
              this.resolveCompletes = resolve;
              this.rejectCompletes = reject;
            }.bind(this));
          }
        },
        /**
         * True when the recaptcha has been moved in body
         */
        inBody: {
          type: Boolean,
          value: false,
          readOnly: true,
          notify: true
        },
        /**
         * Force the recaptcha to be in body, event if it has no shadow root
         */
        forceInBody: {
          type: Boolean,
          value: false
        },
        /**
         * Hide the recaptcha badge. Use this property on your own risk, it may be against google policy.
         */
        hidden: {
          type: Boolean,
          value: false,
          reflectToAttribute: true
        },
        /**
         * Forces the widget to render in a specific language. Auto-detects the
         * user's language if unspecified.
         * See https://developers.google.com/recaptcha/docs/language for
         * available language code.
         * For now, it is a one time configuration. The first recaptcha find
         * defines the language for all others.
         */
        lang: {
          type: String,
          value: '',
          notify: true
        },
        /**
         * The computed size recaptcha parameter.
         */
        _size: {
          type: String,
          computed: '_compute_size(invisible, compact)'
        },
        /**
        * The computed theme recaptcha parameter.
        */
        _theme: {
          type: String,
          computed: '_compute_theme(dark)'
        },
        /**
        * The computed type recaptcha parameter.
        */
        _type: {
          type: String,
          computed: '_compute_type(audio)'
        },
      };
    }

    static get observers() {
      return [
        '_autoRenderIfNeeded(manualRender, libraryLoaded, container, sitekey)',
        '_updateScrollListener(invisible, badge, fitOnScrollDisabled, container)',
        '_updatePosition(inBodyDisabled, forceInBody, container)',
        '_refitIfNeeded(inBody, fitOnScrollDisabled, hidden, container, rendered)',
        '_updateContainerZIndex(inBody, inBodyZIndex, container)',
        '_fetchLibrary(lang)'
      ];
    }

    _updateContainerZIndex(inBody, inBodyZIndex, container) {
      if (!(inBody == undefined || inBodyZIndex == undefined || container == undefined)) {
        container.style.zIndex = inBodyZIndex == null || isNaN(inBodyZIndex) || !inBody ? null : inBodyZIndex;
      }
    }

    _fetchLibrary(lang) {
      if (window.fetchingGrecaptchaLibrary || window.grecaptcha) return;
      let script = document.createElement('script');
      let hl = !!lang ? '&hl=' + lang : '';
      let apiUrl = 'https://www.google.com/recaptcha/api.js?onload=onloadGrecaptchaCallback&render=explicit' + hl;
      script.setAttribute('async', '');
      script.setAttribute('id', 'grecaptchaLibrary');
      script.setAttribute('defer', '');
      script.setAttribute('src', apiUrl);
      script.onerror = function() {
        window.fetchingGrecaptchaLibrary = false;
      };
      window.fetchingGrecaptchaLibrary = true;
      document.head.appendChild(script);
     }

    _hasShadowParent(element) {
      while(element && element.parentNode && (element = element.parentNode)) {
        if(element instanceof ShadowRoot) return true;
      }
      return false;
    }

    attached() {
      let container = this.$$('.recaptcha-container');
      container.fitInto = this;
      this.container = container;
      window.addEventListener('grecaptcha-loaded', function() {
        this._setLibraryLoaded(true);
      }.bind(this));
    }

    detached() {
       // TODO: remove event listener
     }

     _compute_size(invisible, compact) {
       return invisible ? 'invisible' : (compact ? 'compact' : 'normal');
     }

     _compute_theme(dark) {
       return dark ? 'dark' : 'light';
     }

     _compute_type(audio) {
       return audio ? 'audio' : 'image';
     }

     _computeRendered(widgetId) {
       return !isNaN(widgetId);
     }

     _refitIfNeeded(inBody, fitOnScrollDisabled, hidden, container) {
        if (container && inBody && !fitOnScrollDisabled && !hidden) {
          container.refit();
        }
      }

      _updateScrollListener(invisible, badge, fitOnScrollDisabled, container) {
        if (container) {
          container.toggleScrollListener(!fitOnScrollDisabled && !invisible || badge === 'inline');
        }
      }

      _updatePosition(inBodyDisabled, forceInBody, container, rendered) {
        if (!container) return;
        if (!inBodyDisabled) {
          if (!this.inBody && (forceInBody || this._hasShadowParent(this))) {
            // Append to body
            document.body.appendChild(container);
            this._setInBody(true);
          }
        } else if (this.inBody) {
          // Remove from body
          this.root.appendChild(container);
          container.resetFit();
          this._setInBody(false);
        }
      }

      _autoRenderIfNeeded(manualRender, libraryLoaded, container) {
        console.log("AUTORENDER NEEDED!!");
          if(!manualRender && libraryLoaded && container) {
            console.log("INSIDE CONDITION!!!");
            this.render();
          }
        }

        getResponse() {
        if (this.rendered) {
          return window.grecaptcha.getResponse(this.widgetId);
        }
      }

      execute() {
         if (this.rendered && this.invisible) {
           window.grecaptcha.execute(this.widgetId);
           return this.completes;
         }
       }

       reset() {
         if (this.rendered) {
           this._setValue('');
           this._setCompletes(new Promise(function (resolve, reject) {
             this.resolveCompletes = resolve;
             this.rejectCompletes = reject;
           }.bind(this)));
           window.grecaptcha.reset(this.widgetId);
         }
       }

       render() {
         if (!this.rendered && this.sitekey) {
           console.log("INSIDE RENDER!!!");
           console.log(this.container);
           let options = {
             'sitekey': this.sitekey,
             'size': this._size,
             'tabindex': this.tabIndex,
             'callback': this.responseCallback.bind(this),
             'expired-callback': this.expiredCallback.bind(this),
             'theme': this._theme,
             'type': this._type,
             'badge': this.badge
           };

           console.log(options);

           this._setWidgetId(window.grecaptcha.render(this.container, {
             'sitekey': this.sitekey,
             'size': this._size,
             'tabindex': this.tabIndex,
             'callback': this.responseCallback.bind(this),
             'expired-callback': this.expiredCallback.bind(this),
             'theme': this._theme,
             'type': this._type,
             'badge': this.badge
           }));
         }
       }

       _getValidity() {
         return !!this.value;
       }


  }

window.customElements.define('google-recaptcha', GoogleRecaptcha);
