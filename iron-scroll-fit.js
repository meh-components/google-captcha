import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import {mixinBehaviors} from '@polymer/polymer/lib/legacy/class.js';
import {IronFitBehavior} from '@polymer/iron-fit-behavior/iron-fit-behavior.js';
import {IronScrollTargetBehavior} from '@polymer/iron-scroll-target-behavior/iron-scroll-target-behavior.js';

/**
 * `google-recaptcha`
 * A captcha using the google library
 *
 * @customElement
 * @polymer
 * @demo demo/index.html
 */
class IronScrollFit extends mixinBehaviors([IronFitBehavior, IronScrollTargetBehavior], PolymerElement) {

  _scrollHandler(e) {
    this.fit();
  }
}

window.customElements.define('iron-scroll-fit', IronScrollFit);
